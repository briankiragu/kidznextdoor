$(document).ready(function () {
    "use strict";

    $(".dropdown-button").dropdown({
        hover:  true
    });

    // Initialize collapse button
    $(".button-collapse").sideNav();

    // Initialize collapsible (uncomment the line below if you use the dropdown variation)
    $('.collapsible').collapsible();

    $('.slider').slider({
        full_width: true
    });

    $('.parallax').parallax();

    $('.carousel').carousel();

    $(function () {
        $('a[href*="#"]:not([href="#"])').click(function () {
            if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });
});
