from django.conf.urls import url

from . import views

app_name = 'main'
urlpatterns = [
    # http://localhost/
    url(r'^$', views.IndexView, name='index'),

    # http://localhost/about/
    url(r'^about/$', views.AboutView, name='about'),

    # http://localhost/admissions/
    url(r'^admissions/$', views.AdmissionsView, name='admissions'),

    # http://localhost/dates/
    url(r'^dates/$', views.DatesView, name='dates'),

    # http://localhost/contact/
    url(r'^contact/$', views.ContactView, name='contact'),
]
