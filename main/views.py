from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404, redirect
from django.core.mail import send_mail, BadHeaderError
from django.core.mail import EmailMessage
from django.utils import timezone
from django.views import generic
from django.http import HttpResponse, HttpResponseRedirect


# Create your views here.
def IndexView(request):
    context = {
        'main': 'main'
    }
    return render(request, 'main/index.html', context)


def AboutView(request):
    context = {
        'main': 'main'
    }
    return render(request, 'main/about.html', context)


def AdmissionsView(request):
    context = {
        'main': 'main'
    }
    return render(request, 'main/admissions.html', context)


def DatesView(request):
    context = {
        'main': 'main'
    }
    return render(request, 'main/dates.html', context)


def ContactView(request):
    context = {
        'main': 'main'
    }

    if request.POST:
        name = request.POST['name']
        email = request.POST['email']
        subject = request.POST['subject']
        message = request.POST['message']

        if subject and message and email:
            try:
                send_mail(
                    subject,
                    name + ' (' + email + ') says: ' + message,
                    email,
                    ['karisbrian1@gmail.com'],
                    fail_silently=False,
                )
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            return HttpResponseRedirect('/contact/')
        else:
            # In reality we'd use a form class
            # to get proper validation errors.
            return HttpResponse('Make sure all fields are entered and valid.')

    return render(request, 'main/contact.html', context)
